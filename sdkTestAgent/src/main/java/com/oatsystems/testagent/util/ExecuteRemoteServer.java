package com.oatsystems.testagent.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ResourceBundle;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class ExecuteRemoteServer {
	public static String runCommand(String command) throws Exception {
		String output =null;
		ResourceBundle rb = ResourceBundle.getBundle("testconfig");
		String connectIP = rb.getString("serverIP");
		String user = rb.getString("serverUser");
		String password = rb.getString("serverPass");
try{
		// Setup ssh session with endpoint
		System.out.println("starting connection with " + connectIP);
		Connection connection = new Connection(connectIP);
		System.out.println("connection object created..");
		connection.connect();
		connection.authenticateWithPassword(user, password);
		Session session = connection.openSession();
		InputStream stdout = new StreamGobbler(session.getStdout());
		BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(stdout));
		System.out.println("connected");
		
		// Run command
		String tempCommand = command;
		System.out.println("sending command: " + tempCommand);
		session.execCommand(tempCommand);
		

		// Get output
		StringBuffer sb = new StringBuffer();
		while (true) {
			String line = stdoutReader.readLine();
			if (line == null)
				break;
			sb.append(line + "\n");
		}
	     output = sb.toString();
	/*	System.out.println("" + output);*/
		System.out.println("got output: " + output);
		stdout.close();
}
catch(Exception e){
	System.out.println(e.getMessage());
}
		return output;
	}
	
	/*public static String sendFileAndRunCommand(String file,String rmtefile) throws Exception {
		String output =null;
		ResourceBundle rb = ResourceBundle.getBundle("testconfig");
		String connectIP = rb.getString("serverIP");
		String user = rb.getString("serverUser");
		String password = rb.getString("serverPass");
try{
		// Setup ssh session with endpoint
		System.out.println("starting connection with " + connectIP);
		Connection connection = new Connection(connectIP);
		System.out.println("connection object created..");
		connection.connect();
		connection.authenticateWithPassword(user, password);
		Session session = connection.openSession();
		InputStream stdout = new StreamGobbler(session.getStdout());
		BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(stdout));
		System.out.println("connected");
		System.out.println("coping files");
		String[] localfile={file};
		String[] remotefile={rmtefile};
		 final SCPClient scp_client = new SCPClient(connection);
		 scp_client.put(localfile,remotefile,"/usr/local/OATxpr/ofs/conf","0600");
		 System.out.println("copied");

		// Run command
		String tempCommand = "cd /home/user;yes|cp -rf "+remotefile[0]+ " /usr/local/OATxpr/ofs/conf; cd /usr/local/OATxpr/ofs/conf;./propertyChange.sh";
				
		System.out.println("sending command: " + tempCommand);
		session.execCommand(tempCommand);
	
		 
		 
			
			System.out.println("sending command: " + tempCommand);
			session.execCommand(tempCommand);

		// Get output
		StringBuffer sb = new StringBuffer();
		while (true) {
			String line = stdoutReader.readLine();
			if (line == null)
				break;
			sb.append(line + "\n");
		}
	     output = sb.toString();
		System.out.println("" + output);
		System.out.println("got output: " + output);
		stdout.close();
}
catch(Exception e){
	System.out.println(e.getMessage());
}
		return output;
	}*/
}