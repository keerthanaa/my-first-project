package com.oatsystems.testagent.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

public class TestCaseUtil {
	private static Connection con = null;
	static String host;
	static String dbIP;
	static String dbServiceName;
	static String dbPort;
	static String dbUser;
	static String dbPass;
	static ResourceBundle rb = ResourceBundle.getBundle("testconfig");
	public TestCaseUtil() {
		dbIP = rb.getString("dbIP");
		dbServiceName = rb.getString("dbServiceName");
		dbPort = rb.getString("dbPort");
		dbUser = rb.getString("dbUser");
		dbPass = rb.getString("dbPass");
	}
	
	public Connection jdbcConnect() throws Exception {
		StringBuilder dbConnStr = new StringBuilder("jdbc:oracle:thin:@").append(dbIP).append(':').append(dbPort).append(':').append(dbServiceName);
		String dbUrl = dbConnStr.toString();
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(
					dbUrl, dbUser,
					dbPass);
			return con;
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			return con;
		}
	}
	
	public String getPropertyValue(String propertyName) throws Exception {
		try{
			return rb.getString(propertyName);
		} catch (Exception ex) {
			throw ex;
		}
		
	}
}