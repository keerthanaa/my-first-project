package com.oatsystems.testagent.Tests;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.oatsystems.testagent.util.TestCaseUtil;

public class MessagingSendAPITest {

	private static Connection con = null;
	private static Statement st = null;
	private static ResultSet rs = null;
	
	public static void main(String[] args) throws IOException {
		
		connect("http://localhost:8080/sdkTestAgent/rest/executeTest?methodName=startstopwifi&param=off");
		try{
			String query = null;
			if(args != null) {
			query = args[0];
			}
			int count = executeQuery(query);
			} catch(Exception ex) {
				System.out.println("Error in fetching the HH messageCount "
					+ ex.toString());
			}
	}
	/**
	 * Execute the query sent and fetch the result
	 * @param query
	 * @throws IOException 
	 * @throws Exception
	 */
	
	public static HttpURLConnection connect(String URL) throws IOException  {
		//SSLconnection.testconnection();
		URL url = new URL(URL);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		//conn.setRequestProperty("Accept", "application/json");
		conn.setDoOutput(true);
		conn.connect();
		//int responseCode = conn.getResponseCode();
		//System.out.println("Response Vikram Code :" + responseCode);
		//conn.setRequestProperty("Accept", "URL");
		return conn;
	}
	
	public static int executeQuery(String query) throws Exception{	
		TestCaseUtil util = new TestCaseUtil();
		con = util.jdbcConnect();
		st = con.createStatement();
		rs = st.executeQuery(query);
		rs.next();
		int count = rs.getInt(1);
		System.out.println(count);
		rs.close();
		st.close();
		con.close();
		return count;
	}
	
	public static void executeMultipleQuery(String query) throws Exception{	
		TestCaseUtil util = new TestCaseUtil();
		con = util.jdbcConnect();
		st = con.createStatement();
		String seperate[]=query.split(",");

		for(int index=0;index<seperate.length;index++)
		{
			System.out.println(seperate[index]);
		st.executeUpdate(seperate[index]);
		}
		st.close();
		con.close();
	}
}
