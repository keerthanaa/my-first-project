package com.oatsystems.testagent.controller;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.oatsystems.testagent.Tests.MessagingSendAPITest;
import com.oatsystems.testagent.util.ExecuteRemoteServer;
import com.oatsystems.testagent.util.TestCaseUtil;
 
@RestController
@RequestMapping(value = "/rest", produces={"application/json"})
public class TestAgentController {
	/*
	 * Takes the method parameter to execute the script and passes the 
	 * second parameter to the script and returns the result
	 */
	
	@RequestMapping(value = "/executeTest", method = RequestMethod.GET )
	@ResponseBody
	public StringJSONResponse executeTest(@RequestParam(value = "methodName", required = true) String methodName, @RequestParam(value = "param", required = false) String param) {
		
	
		//System.out.println("inside rest controller method"+methodName+"param"+param);
		StringBuilder sb = new StringBuilder();
		
		String scriptName = null;
		if (methodName != null) {
			if(param.equalsIgnoreCase("MissingAndExtraCounts")){
				scriptName = lookupForScript(methodName+"MEC");
			}
			else if(param.equalsIgnoreCase("MissingCount")){
				scriptName = lookupForScript(methodName+"MC");
			}
			else if(param.equalsIgnoreCase("true")){
				scriptName = lookupForScript(methodName+"T");
			}
			else if(param.equalsIgnoreCase("false")){
				scriptName = lookupForScript(methodName+"F");
			}
			else{
			scriptName = lookupForScript(methodName);
			}
			//System.out.println(scriptName);
		}
		if(scriptName == null) {
			sb.append("Valid script not found to execute the test.Please check the config file");
		}
	
		switch(methodName){

		case "cleanInventory":
		{
			StringJSONResponse sr = null;
			  try {
				 String query="update ship_unit set ship_unit_receipt_id=null,update ship_unit_receipt set process_id=null,delete from object_assn_verification,delete from object_product_history,delete from object_assn,delete from object,delete from process_error_log,delete from counted_item,delete from expected_item,delete from expected_product,delete from cc_detail,delete from filter,delete from process,delete from hh_message_info,delete from int_inv_adj_data_staging,delete from int_inv_adj_data,delete from ext_inv_adj_data_staging,delete from ext_inv_adj_data,delete from adjusted_inventory,delete from ship_unit_state,delete from ship_unit_receipt,delete from ship_unit,delete from generic_shipment,delete from DC_PROCESS_BOX_DETAIL,delete from dc_process_session_detail,commit";
				  System.out.println("Query-"+query);
				  MessagingSendAPITest.executeMultipleQuery(query);
			   sr = new TestAgentController.StringJSONResponse("success");
			  } catch (Exception e) {
			   sr = new TestAgentController.StringJSONResponse(e.toString());
			   e.printStackTrace();
			  }
			  return sr;
		}

		case "sendmessage":
		{
			StringJSONResponse sr = null;
			  try {
				  System.out.println("Query-"+param);
			   sr = new TestAgentController.StringJSONResponse(""+MessagingSendAPITest.executeQuery(param));
			  } catch (Exception e) {
			   sr = new TestAgentController.StringJSONResponse(e.toString());
			   e.printStackTrace();
			  }
			  return sr;
		}
	
		case "startexpress" :
		{
			
			try {
				// Execute the shell script with the command
				sb.append(ExecuteRemoteServer.runCommand(scriptName+" "+param));
					}
			 catch(Exception ex) {
				sb.append("Error executing the Script: " + ex.toString());	
			}
			
			//System.out.println("Script output: " + sb.toString()); 
			StringJSONResponse sr = new TestAgentController.StringJSONResponse("success");
			return sr;
		}
		case "accuracycalcmode":
		case "adjustinventory":
		{
			try {
				// Execute the shell script with the command
				sb.append(ExecuteRemoteServer.runCommand(scriptName));
					}
			 catch(Exception ex) {
				sb.append("Error executing the Script: " + ex.toString());	
			}
			/*try {
				String localfile=scriptName;
				String remotefile="propertyChange.sh";
				// Execute the shell script with the command
				sb.append(ExecuteRemoteServer.sendFileAndRunCommand(localfile,remotefile));
					}
			 catch(Exception ex) {
				sb.append("Error executing the Script: " + ex.toString());	
			}
			*/
			//System.out.println("Script output: " + sb.toString()); 
			StringJSONResponse sr = new TestAgentController.StringJSONResponse("success");
			return sr;
			
		}
		case "restartwifi"  : 
		case "startstopwifi":
		{
			
			String commands[] = new String[]{scriptName, param};
			BufferedReader reader = null;
			try {
				// Execute the shell script with the command
				
				Runtime runTime = Runtime.getRuntime();
				Process p = runTime.exec(commands);
				reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = reader.readLine();
				while(line != null){
					sb.append(line);
					line = reader.readLine();
				}
			} catch(Exception ex) {
				sb.append("Error executing the Script: " + ex.toString());	
			}
			finally{
				try {
					if(reader != null){
					reader.close();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//System.out.println("Script output: " + sb.toString()); 
			StringJSONResponse sr = new TestAgentController.StringJSONResponse(sb.toString());
			return sr;
		}
		default :
		{
			StringJSONResponse sr = new TestAgentController.StringJSONResponse("No such method found");
			return sr;
		}
		}
	}
	/**
	 * Looks up for the script to be executed and returns the same
	 * @param methodName
	 * @return
	 */
	String lookupForScript(String methodName) {
		TestCaseUtil util = new TestCaseUtil();
		String scriptName = null;
		String os = null;
		try {
			os=util.getPropertyValue("os");
			//System.out.println("fething property for "+os+" machine");
			if(os.equals("windows"))
			scriptName = util.getPropertyValue(methodName+"win");
			else
			scriptName = util.getPropertyValue(methodName+"ubu");
		} catch (Exception ex) {
			System.out.println("Error in fetching the property value for" + methodName);
		}
		return scriptName;
	}
	
	/**
	 *  String Json Serial Object Returned from the sdkTestAgent.
	 */
	public static class StringJSONResponse {
		private final String response;
		StringJSONResponse(String response){
			this.response = response;
		}
		public String getString() {
			return response;
		}
	}
}

